from pydantic import BaseSettings, EmailStr
from odmantic import Model
from odmantic.bson import Int64


class Settings(BaseSettings):
    mongodb_host: str
    mongodb_database: str = "ofr"

    smtp_host: str
    smtp_login: str
    smtp_password: str
    smtp_port: int = 587
    smtp_use_tls: bool = False
    smtp_use_starttls: bool = False

    update_interval: int = 60


class Subscription(Model):
    email: EmailStr
    channel: str
    device_id: str
    last_known_id: str
    chat_id: Int64  # Telegram recently started using 64 bit IDs

    class Config:
        collection = "subscriptions"
