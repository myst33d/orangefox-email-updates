import asyncio
import aiosmtplib
import logging
import time
from email.message import EmailMessage

from jinja2 import Environment, PackageLoader
from orangefoxapi import OrangeFoxAsyncAPI
from motor.motor_asyncio import AsyncIOMotorClient
from odmantic import AIOEngine

from .definitions import Settings, Subscription

logging.basicConfig(
    level="INFO",
    format="%(asctime)s %(levelno)s %(message)s",
)
log = logging.getLogger("omail")


async def main():
    log.info("Starting omail")

    try:
        settings = Settings(_env_file="config/configuration.env")
    except Exception as e:
        log.info(e)
        log.info("Please check your configuration in config/configuration.env file")  # yapf: disable # noqa: E501
        return

    template = Environment(loader=PackageLoader(__name__)).get_template("email.html")  # yapf: disable # noqa: E501

    log.info("Connecting to MongoDB")

    client = AsyncIOMotorClient(settings.mongodb_host)
    engine = AIOEngine(motor_client=client, database=settings.mongodb_database)

    api = OrangeFoxAsyncAPI()

    log.info("Connecting to SMTP server")

    smtp = aiosmtplib.SMTP(
        hostname=settings.smtp_host,
        port=settings.smtp_port,
        use_tls=settings.smtp_use_tls,
    )

    try:
        await smtp.connect()
    except Exception as e:
        log.error(e)
        log.error("Connection to the SMTP server failed")
        return

    if settings.smtp_use_starttls:
        await smtp.starttls()

    try:
        await smtp.login(settings.smtp_login, settings.smtp_password)
    except Exception as e:
        log.error(e)
        log.error("SMTP server rejected your credentials, make sure your credentials and your TLS configuration are valid")  # yapf: disable # noqa: E501
        return

    while True:
        start_time = time.time()
        log.info("Processing subscriptions")

        async for subscription in engine.find(Subscription):
            if subscription.channel == "all":
                updates = await api.updates(
                    subscription.last_known_id,
                    device_id=subscription.device_id,
                )
            else:
                updates = await api.updates(
                    subscription.last_known_id,
                    release_type=subscription.channel,
                    device_id=subscription.device_id,
                )

            if updates.data:
                # Get latest update based on the date (API returns unsorted results)
                lastest_update = max(updates.data, key=lambda x: x.date)

                if subscription.last_known_id != lastest_update.id:
                    # Update last_known_id in subscription data
                    subscription.last_known_id = lastest_update.id

                    # Get device data
                    device_data = await api.device(id=subscription.device_id)

                    # Prepare email
                    message = EmailMessage()
                    message["From"] = settings.smtp_login
                    message["To"] = subscription.email
                    message["Subject"] = f"New OrangeFox Recovery updates for {device_data.full_name}!"  # yapf: disable # noqa: E501
                    message.set_content(
                        template.render(
                            device_name=device_data.full_name,
                            download_url=device_data.url,
                            updates=updates,
                        ),
                        subtype="html",
                    )

                    # Send updates email
                    await smtp.send_message(message)

                    # Save subscription data to the database
                    await engine.save(subscription)

        log.info(f"Processed in {round(time.time() - start_time, 2)} seconds")

        await asyncio.sleep(settings.update_interval)


asyncio.run(main())
